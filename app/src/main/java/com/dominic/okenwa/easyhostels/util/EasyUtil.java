package com.dominic.okenwa.easyhostels.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Vibrator;
import android.preference.PreferenceManager;

/**
 * Created by live on 3/9/16.
 */
public class EasyUtil {
    public static void vibrate(Context c, int mill){
        Vibrator v = (Vibrator) c.getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(mill);
    }

    public static boolean checkFirstTime(Activity act) {
        boolean status = false;
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(act);
        boolean check = settings.getBoolean("first_time", true);
        if (check) {
            status = true;
            settings.edit().putBoolean("first_time", false).commit();
        }
        return status;
    }

    public static boolean checkFirstTimeTutorial(Activity act) {
        boolean status = false;
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(act);
        boolean check = settings.getBoolean("first_time_tutorial", true);
        if (check) {
            status = true;
            settings.edit().putBoolean("first_time_tutorial", false).commit();
        }
        return status;
    }

    public static boolean checkLoggedIn(Activity act) {
        boolean status = false;
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(act);
        int uid = settings.getInt("current_user_id", -1);
        if(uid > -1){
            status = true;
        }
        return status;
    }


}
