package com.dominic.okenwa.easyhostels.activity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.dominic.okenwa.easyhostels.R;
import com.dominic.okenwa.easyhostels.helpers.RestStatic;
import com.klinker.android.sliding.SlidingActivity;

import java.net.URL;

public class HostelDetailView extends SlidingActivity {

    private View.OnClickListener onClickListener;

    @Override
     public void init(Bundle savedInstanceState) {
        setContent(R.layout.activity_hostel_detail_view);
        setTitle("");
        setPrimaryColors(
                getResources().getColor(R.color.colorPrimary),
                getResources().getColor(R.color.colorPrimaryDark)
        );



        URL url = null;
        try {
            url = new URL("http://vvuconnect.net/e-service/images/hostel/"+RestStatic.current_hostel.getId()+".jpg");
            Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            setImage(bmp);


        } catch (java.io.IOException e) {
            e.printStackTrace();
        }

        String name = RestStatic.current_hostel.getDisplay_name();
        String address = RestStatic.current_hostel.getAddress();
        String desc = RestStatic.current_hostel.getShort_description();

        TextView txtName = (TextView) findViewById(R.id.txtHostelName);
        TextView txtAdress = (TextView)findViewById(R.id.txtHostelAddress);
        TextView txtDescription = (TextView)findViewById(R.id.txtDescription);

        txtName.setText(name);
        txtAdress.setText(address);
        txtDescription.setText(desc);


        ImageButton btnEmail = (ImageButton)findViewById(R.id.btnEmail);
        btnEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RestStatic.sendEmail(HostelDetailView.this,RestStatic.current_hostel.getContact_email());
            }
        });

        ImageButton btnPhone = (ImageButton)findViewById(R.id.btnCall);
        btnPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RestStatic.callNumber(HostelDetailView.this, RestStatic.current_hostel.getContact_phone());
            }
        });

        ImageButton btnDirections = (ImageButton)findViewById(R.id.btnRoute);
        btnDirections.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RestStatic.getDirections(HostelDetailView.this, RestStatic.current_hostel.getLat(), RestStatic.current_hostel.getLng());
            }
        });


    }
}
