package com.dominic.okenwa.easyhostels.graphics;

import android.graphics.Color;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mingle.sweetpick.Effect;

/**
 * Created by live on 3/13/16.
 */
public class CustomDimEffect implements Effect {

    private float Value;

    public CustomDimEffect(float value) {
        Value = value;
    }

    public void setValue(float value) {
        Value = value;
    }

    @Override
    public float getValue() {
        return Value;
    }

    @Override
    public void effect(ViewGroup vp, ImageView view) {
        view.setBackgroundColor(Color.argb((int) (150 * Value), 0, 0, 0));
    }
}